# -*- coding: UTF-8 -*-
# alter table users convert to character set utf8 collate utf8_unicode_ci;
# alter table boards convert to character set utf8 collate utf8_unicode_ci;
# alter table commands convert to character set utf8 collate utf8_unicode_ci;

import os
import datetime

import psycopg2
import yaml
from sqlalchemy import create_engine, Table, Column, Integer, String, Boolean, Sequence, ForeignKey, DateTime
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine.url import make_url

def loadconfig(filename):
    with open(filename, 'r') as stream:
        configs = yaml.load(stream)
    return configs


CONFIGFILE = "config.yaml"
CONFIGS = loadconfig(CONFIGFILE)
BASE_DIR = os.path.abspath(os.path.dirname(__file__))
LANGUAGE_PACK = os.path.join(BASE_DIR, CONFIGS['LANGUAGE_PACK'])

def pconn():
    url = make_url(CONFIGS['PSQL_URL'])
    return psycopg2.connect(database=url.database, 
                            user=url.username, 
                            password=url.password)

ENGINE = create_engine('postgresql://', creator=pconn)

# If you use MySQL, uncomment this and set proper configs in config.yaml
# ENGINE = create_engine(CONFIGS['MYSQL_URL'], encoding='utf8')

Session = sessionmaker(bind=ENGINE)
session = Session()
Base = declarative_base()

association_table = Table('user_board', Base.metadata,
                          Column('user_id', Integer, ForeignKey('users.id')),
                          Column('board_id', Integer, ForeignKey('boards.id'))
                         )

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, Sequence('user_id_seq'), primary_key=True)
    uuid = Column(Integer)
    username = Column(String(20))
    state = Column(Integer)
    lang = Column(String(3))
    current_board_id = Column(Integer, ForeignKey('boards.id'), nullable=True)

    boards = relationship("Board",
                          secondary=association_table,
                          back_populates="users")
    current_board = relationship('Board', 
                                 foreign_keys='User.current_board_id')
    requests = relationship('Request', backref='user', lazy='dynamic')

    def __init__(self, uuid, username):
        self.uuid = uuid
        self.username = username
        self.state = 0
        self.lang = 'eng'

    def as_dict(self):
        return {'table': self.__tablename__,
                'id': int(self.id), 
                'username': self.username}


class Board(Base):
    __tablename__ = "boards"
    id = Column(Integer, Sequence('board_id_seq'), primary_key=True)
    eid = Column(String(256))
    title = Column(String(40))
    state = Column(Integer)

    commands = relationship('Command', backref='board', lazy='dynamic')
    users = relationship("User",
                         secondary=association_table,
                         back_populates="boards")

    def __init__(self, eid, title):
        self.eid = eid
        self.title = title
        self.state = 1

    def as_dict(self):
        return {'table': self.__tablename__,
                'id': int(self.id), 
                'eid': self.eid, 
                'title': self.title}


class Command(Base):
    __tablename__ = "commands"
    id = Column(Integer, Sequence('command_id_seq'), primary_key=True)
    board_id = Column(Integer, ForeignKey('boards.id'))
    title = Column(String(40))
    command = Column(String(100))

    requests = relationship('Request', backref='command', lazy='dynamic')

    def __init__(self, title, command):
        self.title = title
        self.command = command

    def as_dict(self):
        return {'table': self.__tablename__,
                'id': int(self.id), 
                'board_id': int(self.board_id), 
                'title': self.title, 
                'command': self.command}

class Request(Base):
    __tablename__ = "requests"
    user_id = Column(Integer, ForeignKey('users.id'), primary_key=True)
    command_id = Column(Integer, ForeignKey('commands.id'), primary_key=True)
    datetime = Column(DateTime)

    def __init__(self, command_id):
        self.command_id = command_id
        self.datetime = datetime.datetime.now()

class Change(Base):
    __tablename__ = "changes"
    id = Column(Integer, Sequence('change_id_seq'), primary_key=True)
    change_type = Column(Integer)
    data = Column(String(1000))
    status = Column(Integer)

    def __init__(self, change_type, data):
        self.change_type = change_type
        self.data = data
        # status = 0 - changed here, not synhronized
        # status >= 1 - changed here, synchronized
        # status = -1 - changed other place, synchronized here
        self.status = 0

    def as_dict(self):
        return {'table': self.__tablename__,
                'id': self.id,
                'change_type': self.change_type,
                'status': self.status,
                'data': self.data}


Base.metadata.create_all(ENGINE)

# Adding virtual board
if not session.query(Board).get(1):
    board = Board("VBOARD1", "VBOARD1")
    command = Command("Get Temperature in Living Room", "get_temperature")
    board.commands.append(command)
    command = Command("Get Board Status", "get_status")
    board.commands.append(command)
    session.add(board)
    session.commit()
