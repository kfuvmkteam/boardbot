# -*- coding: UTF-8 -*-
"""
    Telegram bot used as manager of smart house
"""

import logging
import datetime
import requests
import json

import paho.mqtt.client as mqtt
import telegram
from telegram.ext import CommandHandler, Updater, MessageHandler, Filters, CallbackQueryHandler
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Job

from models import session, User, Board, Command, Request, Change, CONFIGS, LANGUAGE_PACK

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    filename='log.log',
                    level=logging.ERROR)

def load_lang_pack(filename):
    """
        Loading language pack for localization support
    """
    with open(filename, 'r') as stream:
        data = stream.read().decode('utf-8')
    result = {}
    header = []
    languages = []
    for line in data.split('\n'):
        items = line.split('\t')
        if not header:
            header = items
            langs = items[1:]
            continue
        text = dict(zip(header, items))
        if text['code'] == 'language':
            languages = zip(langs, items[1:])
        result[text['code']] = text

    if not languages:
        languages = zip(langs, langs)

    return result, languages

STRINGS, LANGUAGES = load_lang_pack(LANGUAGE_PACK)
logging.info("Language pack's `%s` length: %s", LANGUAGE_PACK, len(STRINGS.keys()))

def gettext(text, lang='eng', params={}):
    if text not in STRINGS:
        return text.replace('\\n', '\n') % params
    else:
        return STRINGS[text].get(lang, STRINGS[text].get('eng', text)).replace('\\n', '\n') % params


def on_connect(client, userdata, flags, rc):
    logging.info("Connected with result code %s", str(rc))
    client.subscribe([("board/+/response/+", 1), ("board/+/sensors/+", 0)])


def on_board_response(client, userdata, msg):
    logging.info("***** Get message from '%s': '%s'", msg.topic, str(msg.payload))
    headers = ['root', 'board_eid', 'c', 'command']
    message = dict(zip(headers, msg.topic.split('/')))
    try:
        reqs = session.query(Request).join(Command, Request.command_id == Command.id)\
                          .join(Board, Command.board_id == Board.id)\
                          .filter(Board.eid == message['board_eid'])\
                          .filter(Command.command == message['command']).all()
        if reqs and client.bot:
            for request in reqs:
                client.bot.sendMessage(chat_id=request.user.uuid,
                                       text=gettext("board_response",
                                                    lang=request.user.lang,
                                                    params={"board": message['board_eid'],
                                                            "command": message['command'],
                                                            "response": str(msg.payload)}))
                session.delete(request)
            session.commit()
    except Exception as err:
        logging.info('***** on_board_response error: %s', str(err))

def on_sensor_message(client, userdata, msg):
    # just used to detect new boards
    logging.info("***** Get message from '%s': '%s'", msg.topic, str(msg.payload))
    headers = ['root', 'board_eid', 's', 'sensor']
    message = dict(zip(headers, msg.topic.split('/')))
    try:
        board = session.query(Board).filter(Board.eid == message['board_eid']).first()
        if not board:
            board = Board(message['board_eid'], message['board_eid'])
            session.add(board)
            session.commit()
            # creates Change to synchronize with other agents
            change = Change(3, json.dumps(board.as_dict()))
            session.add(change)
            session.commit()
            logging.info("New board(id=%s, eid=%s, title=%s)",
                         board.id, board.eid, board.title)
    except Exception as err:
        logging.info('***** on_sensor_message error: %s', str(err))


MQTTCLIENT = mqtt.Client()
MQTTCLIENT.on_connect = on_connect
MQTTCLIENT.message_callback_add("board/+/response/+", on_board_response)
MQTTCLIENT.message_callback_add("board/+/sensors/+", on_sensor_message)

MQTTCLIENT.username_pw_set(CONFIGS['MQTT_USERNAME'], CONFIGS['MQTT_PASSWORD'])
MQTTCLIENT.connect(CONFIGS['MQTT_HOST'], CONFIGS['MQTT_PORT'], 60)
MQTTCLIENT.loop_start()
MQTTCLIENT.bot = None

def get_keyboard(state):
    if state == 0:
        commands = [['/help', '/back']]
    elif state == 1:
        commands = [['/boards', '/help']]
    elif state == 2:
        commands = [['/commands', '/status'], ['/back', '/help']]
    else:
        commands = [[]]

    return telegram.ReplyKeyboardMarkup(commands, resize_keyboard=True)

def login(uuid, username=None):
    """
        Login user
    """
    user = session.query(User).filter(User.uuid == uuid).first()
    if user:
        if username and user.username != username:
            user.username = username
            session.commit()
    else:
        user = session.query(User).filter(User.username == username).first()
        if user:
            user.uuid = uuid
            user.state = 1
            session.commit()
        else:
            user = User(uuid=uuid, username=username)
            user.state = 1
            session.add(user)
            session.commit()
            # adding Change to db with synchronization with others
            change = Change(1, json.dumps(user.as_dict()))
            session.add(change)
            session.commit()
            logging.info("New user(id=%s, uuid=%s, username=%s)",
                         user.id, user.uuid, user.username)
    return user

def start(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)

    # adding virtual board to everybody who starts the bot
    vboard1 = session.query(Board).get(1)
    if vboard1 not in user.boards:
        user.boards.append(vboard1)
        change = Change(7, json.dumps({'user_id': int(user.id), 'board_id': 1}))
        session.add(change)
        session.commit()

    bot.sendMessage(chat_id=update.message.chat_id,
                    text=gettext("hello",
                                 lang=user.lang,
                                 params={'username': user.username}))
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=gettext("info", lang=user.lang),
                    reply_markup=get_keyboard(state=1))

def synchronize():
    print('hello')

def help(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)

    if user.state == 2:
        message = gettext("help2", lang=user.lang)
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=message)
    else:
        synchronize()
        if user.state != 1:
            user.state = 1
            session.commit()
        message = gettext("help1", lang=user.lang)
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=message,
                        reply_markup=get_keyboard(1))

def back(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    if user.state in [1, 2]:
        user.state = 1
        user.current_competition_id = None
        session.commit()
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=gettext("back", lang=user.lang),
                        reply_markup=get_keyboard(1))
    else:
        user.state = 1
        session.commit()
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=gettext("back", lang=user.lang),
                        reply_markup=get_keyboard(1))

def unknown(bot, update):
    """
        Handles unknown commands
    """
    user = login(update.message.from_user.id, update.message.from_user.username)
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=gettext("error_dont_know_command", lang=user.lang))

def change_language(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    if user.state != 1:
        user.state = 1
        session.commit()
    message = gettext("languages_list", lang=user.lang)
    keyboard = [[InlineKeyboardButton(title, callback_data='language|%s' % code)]
                for code, title in LANGUAGES]
    reply_markup = InlineKeyboardMarkup(keyboard)
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=message,
                    reply_markup=reply_markup)

def boards_list(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    if user.state != 1:
        user.state = 1
        session.commit()
    message = gettext("boards_list", lang=user.lang)
    keyboard = [[InlineKeyboardButton(board.title, callback_data='board|%s' % board.id)]
                for board in user.boards]
    reply_markup = InlineKeyboardMarkup(keyboard)
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=message,
                    reply_markup=reply_markup)

def commands_list(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    message = gettext("commands_list", lang=user.lang)
    keyboard = [[InlineKeyboardButton(command.title, callback_data='command|%s' % command.id)]
                for command in user.current_board.commands]
    reply_markup = InlineKeyboardMarkup(keyboard)
    bot.sendMessage(chat_id=update.message.chat_id,
                    text=message,
                    reply_markup=reply_markup)

def board_status(bot, update):
    user = login(update.message.from_user.id, update.message.from_user.username)
    if user.state == 2 and user.current_board:
        command = session.query(Command)\
                         .filter(Command.board_id == user.current_board_id)\
                         .filter(Command.command == 'get_status').first()
        if command:
            user.requests.append(Request(command.id))
            session.commit()
            MQTTCLIENT.publish("board/%s/command" % command.board.eid, command.command)
        else:
            bot.sendMessage(chat_id=update.message.chat_id,
                            text=gettext("board_response",
                                         lang=user.lang,
                                         params={"board": user.current_board.eid,
                                                 "command": "get_status",
                                                 "response": "`get_status` is not set"}))
    else:
        bot.sendMessage(chat_id=update.message.chat_id,
                        text=gettext("error_board_404", lang=user.lang))


def echo(bot, update):
    bot.sendMessage(chat_id=update.message.chat_id,
                    text='%s ?' % update.message.text)

def button(bot, update):
    query = update.callback_query
    bot.answerCallbackQuery(callback_query_id=query.id)
    parameters = query.data.split('|')
    user = login(query.message.chat_id)
    if parameters[0] == 'board':
        board = session.query(Board).get(parameters[1])
        if board and board in user.boards:
            user.state = 2
            user.current_board_id = board.id
            session.commit()
            bot.sendMessage(chat_id=query.message.chat_id,
                            text=gettext("board_status",
                                         lang=user.lang,
                                         params={'status': 'ON'}),
                            reply_markup=get_keyboard(2))
        else:
            bot.sendMessage(chat_id=query.message.chat_id,
                            text=gettext("error_board_404", lang=user.lang),
                            reply_markup=get_keyboard(1))
    elif parameters[0] == 'command':
        command = session.query(Command).get(parameters[1])
        if command and command.board in user.boards:
            try:
                if not session.query(Request).get((user.id, command.id)):
                    user.requests.append(Request(command.id))
                    session.commit()
                MQTTCLIENT.publish("board/%s/command" % command.board.eid, command.command)
            except Exception as err:
                logging.error("Error while sending command, %s", str(err))
                bot.sendMessage(chat_id=query.message.chat_id,
                                text=gettext("error_mqtt_publish",
                                             lang=user.lang,
                                             params={'err': err}))
                session.rollback()

        else:
            bot.sendMessage(chat_id=query.message.chat_id,
                            text=gettext("error_command_404", lang=user.lang),
                            reply_markup=get_keyboard(1))
    elif parameters[0] == 'language':
        user.lang = parameters[1]
        session.commit()
        bot.sendMessage(chat_id=query.message.chat_id,
                        text=gettext("hello",
                                     lang=user.lang,
                                     params={'username': user.username}),
                        reply_markup=get_keyboard(1))

def synchronize_db(bot, job):
# def synchronize_db(bot, update):
    """
        Handles the periodic database synchronizations
    """
    changes = session.query(Change).filter(Change.status == 0).all()
    result = [change.as_dict() for change in changes]

    data = json.dumps({"result": 1, "changes": result})
    logging.info("***** 1) data: %s" % data)

    response = requests.post(CONFIGS['WEB_UI_SYNC'],
                             data=data)
    logging.info("***** 2) responce code: %s" % response.status_code)

    if response.status_code == requests.codes.ok:
        # changes statuses to 'synchronized'
        for change in changes:
            change.status = 1
            session.commit()

        data = response.json()
        logging.info("***** 3) responce json: %s" % data)

        changes = data['changes']

        synchronized = []
        try:
            for change in changes:
                logging.info("***** 3+c) change=%s", change)

                # adding change with status 'notification'

                change_type = change['change_type']

                if change_type == 5:
                    # *5. added command
                    data = json.loads(change['data'])
                    command = Command(title=data['title'], 
                                      command=data['command'])
                    command.id = data['id']
                    command.board_id = data['board_id']
                    session.add(command)
                    session.commit()
                    synchronized.append(change['id'])
                    logging.info('***** Synchronization (id=%s, change_type=%s, data=%s',
                                 change['id'],
                                 change['change_type'],
                                 change['data'])

                elif change_type == 6:
                    # 6. edit command
                    data = json.loads(change['data'])
                    command = session.query(Command).get(data['id'])
                    command.board_id = data['board_id']
                    command.title = data['title']
                    command.command = data['command']
                    new_change = Change(change['change_type'], change['data'])
                    new_change.status = -1
                    session.add(new_change)
                    session.commit()
                    synchronized.append(change['id'])
                    logging.info('***** Synchronization (id=%s, change_type=%s, data=%s',
                                 change['id'],
                                 change['change_type'],
                                 change['data'])
                elif change_type == 9:
                    # 9. delete command
                    data = json.loads(change['data'])
                    command = session.query(Command).get(data['id'])
                    new_change = Change(change['change_type'], change['data'])
                    new_change.status = -1
                    session.add(new_change)
                    session.delete(command)
                    session.commit()
                    synchronized.append(change['id'])
                    logging.info('***** Synchronization (id=%s, change_type=%s, data=%s',
                                 change['id'],
                                 change['change_type'],
                                 change['data'])
                elif change_type == 7:
                    # 7. added user board relationship
                    data = json.loads(change['data'])
                    user = session.query(User).get(data['user_id'])
                    board = session.query(Board).get(data['board_id'])
                    new_change = Change(change['change_type'], change['data'])
                    if board not in user.boards:
                        user.boards.append(board)
                        new_change.status = -1
                    else:
                        new_change.status = -2
                    session.add(new_change)
                    session.commit()
                    synchronized.append(change['id'])
                    logging.info('***** Synchronization (id=%s, change_type=%s, data=%s',
                                 change['id'],
                                 change['change_type'],
                                 change['data'])
                elif change_type == 8:
                    # 8. delete user board relationship
                    data = json.loads(change['data'])
                    user = session.query(User).get(data['user_id'])
                    board = session.query(Board).get(data['board_id'])
                    new_change = Change(change['change_type'], change['data'])
                    if board in user.boards:
                        user.boards.remove(board)
                        new_change.status = -1
                    else:
                        new_change.status = -2
                    session.add(new_change)
                    session.commit()
                    synchronized.append(change['id'])
                    logging.info('***** Synchronization (id=%s, change_type=%s, data=%s',
                                 change['id'],
                                 change['change_type'],
                                 change['data'])

            # send other request to WEB_UI to confirm synchronization
            logging.info('***** Synchronized: %s', synchronized)
            if synchronized:
                data = json.dumps({"result": 1, "synchronized": synchronized})
                response = requests.post(CONFIGS['WEB_UI_CONFIRM_SYNC'],
                                         data=data)
                if response.status_code == requests.codes.ok:
                    logging.info('***** Synchronization confirmed')
                else:
                    logging.error('***** Synchronization confirmation failed with code: %s',
                                  response.status_code)
            else:
                logging.info('***** Synchronization confirmation: no need to confirm')

        except Exception as err:
            logging.error('***** Synchronization failed with exception: %s', err)
    else:
        logging.error('***** Synchronization failed with code: %s', response.status_code)


def request_checker(bot, job):
    dt = datetime.datetime.now() + datetime.timedelta(seconds=30)
    reqs = session.query(Request)\
                      .filter(Request.datetime < dt).all()
    if reqs:
        for request in reqs:
            bot.sendMessage(chat_id=request.user.uuid,
                            text=gettext("board_response",
                                         lang=request.user.lang,
                                         params={"board": request.command.board.eid,
                                                 "command": request.command.command,
                                                 "response": "Not responding"}))
            session.delete(request)
        session.commit()

def error(bot, update, telegram_error):
    logging.info('***** error handler: %s', str(telegram_error))
    if update:
        bot.sendMessage(chat_id=update.callback_query.message.chat_id,
                        text='Oops! I have encountered an error! :(')


def main():
    updater = Updater(token=CONFIGS['TOKEN'])
    MQTTCLIENT.bot = updater.bot

    jober = updater.job_queue
    job_synchronize_db = Job(synchronize_db, 60)
    jober.put(job_synchronize_db, next_t=0.0)

    job_request_checker = Job(request_checker, 30)
    jober.put(job_request_checker, next_t=0.0)

    dispatcher = updater.dispatcher
    dispatcher.add_handler(CommandHandler('start', start))
    # dispatcher.add_handler(CommandHandler('sync', synchronize_db))
    dispatcher.add_handler(CommandHandler('help', help))
    dispatcher.add_handler(CommandHandler('back', back))
    dispatcher.add_handler(CommandHandler('change_language', change_language))

    dispatcher.add_handler(CommandHandler('boards', boards_list))
    dispatcher.add_handler(CommandHandler('commands', commands_list))
    dispatcher.add_handler(CommandHandler('status', board_status))
    dispatcher.add_error_handler(error)

    dispatcher.add_handler(CallbackQueryHandler(button))

    dispatcher.add_handler(MessageHandler([Filters.command], unknown))
    dispatcher.add_handler(MessageHandler([Filters.text], echo))
    updater.start_polling()

if __name__ == '__main__':
    main()
