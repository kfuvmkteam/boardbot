Board Bot
=========

Бот телеграмма для управления платой Intel Edison/Raspberry Pi/etc.


Установка:
----------

Сначала скачаем файлы бота:

```bash
$ git clone https://gitlab.com/kfuvmkteam/boardbot.git
```

После нужно будет установить виртуальное окружение (можно почитать [здесь](http://adw0rd.com/2012/6/19/python-virtualenv/)). 
Потом поставить все используемые модули. Cписок находится в файле requirements.txt

```
$ cd boardbot/
$ pip install -r requirements.txt
```

Затем не забыть настроить конфиг файл:
1. Создать телеграм-бота через [@BotFather](http://telegram.me/BotFather), получить токен
2. Настроить базу данных (MySQL, Postgres и т.д.)
3. Настроить соединение с MQTT Borker'ом

Чтобы запустить бота:

```
$ python bot.py
```


Демо
----

Демо можно посмотреть [тут](http://telegram.me/smart_board_bot)


TODO
----
* Поддержка конфигов
* Доделать синхронизацию БД 
* Дописать README


Контакты:
---------

Ramil Gataullin: ramil.gata@gmail.com